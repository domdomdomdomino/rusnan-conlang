use std::fmt::{self, Display};

#[derive(Debug, PartialEq, PartialOrd)]
struct Sentence<'a> {
    words: Vec<Word<'a>>,
}

impl<'a> Sentence<'a> {
    fn new(ws: Vec<Word<'a>>) -> Sentence {
        Sentence { words: ws }
    }
}

impl<'a> Display for Sentence<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let words = &self.clone().words;
        let string = words
            .into_iter()
            .map(|w| format!("{}", w))
            .collect::<Vec<String>>()
            .join(" ");
        let mut c = string.chars();
        match c.next() {
            Some(a) => write!(
                f,
                "{}",
                a.to_uppercase().collect::<String>() + c.as_str() + "."
            ),
            None => write!(f, "{}", String::new()),
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
struct Word<'a> {
    atoms: Vec<Atom<'a>>,
}

impl<'a> Word<'a> {
    fn new(ws: Vec<Atom<'a>>) -> Result<Word<'a>, ErrWord> {
        let a_word = Word { atoms: ws };

        match a_word.sort().validate() {
            Ok(w) => return Ok(w),
            Err(e) => return Err(e),
        }
    }

    fn create(ws: Vec<Atom<'a>>) -> Word<'a> {
        match Word::new(ws) {
            Ok(v) => v,
            Err(e) => panic!("Panic! Got `{:?}` in the `Work::create()` function.", e),
        }
    }

    fn sort(self) -> Word<'a> {
        let mut aux = self;
        aux.atoms
            .sort_by(|a, b| a.weight.partial_cmp(&b.weight).unwrap());
        return aux;
    }

    fn validate(self) -> Result<Word<'a>, ErrWord> {
        if !self.atoms.windows(2).all(|w| w[0].weight < w[1].weight) {
            return Err(ErrWord::ErrIncompatibleWeights);
        }

        if !self
            .atoms
            .windows(2)
            .all(|w| w[0].category.ne(&w[1].category))
        {
            return Err(ErrWord::ErrIncompatibleCats);
        }

        Ok(self)
    }

    fn make_readable(s: String) -> String {
        let vowels = [
            'a', 'á', 'e', 'é', 'i', 'í', 'o', 'ó', 'u', 'ú', 'y', 'ÿ', 'ŷ',
        ];
        let vowels_p = |c: char| -> bool { vowels.contains(&c) };
        let test = |c1: char, c2: char| -> bool { vowels_p(c1) != vowels_p(c2) };

        // a[x]
        s.chars()
            .collect::<Vec<char>>()
            .chunks(4)
            .map(|c: &[char]| -> String {
                match c {
                    [a, '[', b, ']'] if test(*a, *b) => String::from(format!("{}{}", a, b)),
                    [a, '[', _, ']'] => {
                        format!("{}", a)
                    }
                    ['[', _, ']', a] => {
                        format!("{}", a)
                    }
                    [a, b, c, '['] | [']', a, b, c] => {
                        format!("{}{}{}", a, b, c)
                    }
                    [_, ']', a, b] | [a, b, '[', _] => {
                        format!("{}{}", a, b)
                    }
                    [a, b, c, d] => {
                        format!("{}{}{}{}", a, b, c, d)
                    }
                    ['[', _, ']'] => String::new(),
                    [']', a, b] | [a, b, '['] => format!("{}{}", a, b),
                    [a, b, c] => format!("{}{}{}", a, b, c),
                    [_, ']'] => String::new(),
                    [']', a] => format!("{}", a),
                    [a, b] => format!("{}{}", a, b),
                    ['['] | [']'] => String::new(),
                    [a] => format!("{}", a),
                    _ => String::new(),
                }
            })
            .collect::<Vec<String>>()
            .join("")
    }
}

impl<'a> Display for Word<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let atoms = &self.clone().atoms;
        let string = atoms
            .into_iter()
            .map(|x| format!("{}", x))
            .collect::<Vec<String>>()
            .join("");
        write!(f, "{}", Word::make_readable(string))
    }
}

#[derive(Debug)]
enum ErrWord {
    ErrIncompatibleCats,
    ErrIncompatibleWeights,
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
struct Atom<'a> {
    category: Category<'a>,
    weight: u8,
}

impl<'a> Display for Atom<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.category)
    }
}

impl<'a> Atom<'a> {
    fn new(cat: Category<'a>) -> Atom<'a> {
        match cat {
            Category::Root(_) => Atom {
                category: cat,
                weight: 1u8,
            },
            Category::Adjective(_) => Atom {
                category: cat,
                weight: 255u8,
            },
            Category::Plural(_) => Atom {
                category: cat,
                weight: 100u8,
            },
            Category::Verb(_) => Atom {
                category: cat,
                weight: 100u8,
            },

            Category::Composite(_) => Atom {
                category: cat,
                weight: 50u8,
            },
            Category::Possessive(_) => Atom {
                category: cat,
                weight: 40u8,
            },
            Category::Negation => Atom {
                category: cat,
                weight: 0u8,
            },
            Category::Definite => Atom {
                category: cat,
                weight: 250,
            },
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
enum Category<'a> {
    Root(&'a str),
    Adjective(AdjectiveTypes),
    Plural(PluralTypes),
    Verb(VerbTypes),
    Composite(Box<Vec<Word<'a>>>),
    Possessive((VerbTypes, Box<Vec<Word<'a>>>)),
    Negation,
    Definite,
}

impl<'a> Display for Category<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Category::Root(r) => write!(f, "{}", r),
            Category::Adjective(a) => write!(f, "{}", a),
            Category::Plural(p) => write!(f, "{}", p),
            Category::Verb(v) => write!(f, "{}", v),
            Category::Composite(c) => {
                write!(
                    f,
                    "·{}·",
                    (&**c)
                        .into_iter()
                        .map(|x| format!("{}", x))
                        .collect::<Vec<String>>()
                        .join("·")
                )
            }
            Category::Possessive(b) => {
                let pronoun = match b.0 {
                    VerbTypes::FirstSingular => String::from("ź"),
                    VerbTypes::FirstPluralNonInclusive => String::from("c"),
                    VerbTypes::FirstPluralInclusive => String::from("ć"),
                    VerbTypes::SecondSingular => String::from("t"),
                    VerbTypes::SecondSingularVague => String::from("mók"),
                    VerbTypes::SecondPlural => String::from("el"),
                    VerbTypes::SecondPluralVague => String::from("mŷk"),
                    VerbTypes::ThirdSingular => String::from("w"),
                    VerbTypes::ThirdPlural => String::from("lŷ"),
                    VerbTypes::ThirdPluralVague => String::from("śá"),
                    VerbTypes::FirstGroupNonInclusive => String::from("có"),
                    VerbTypes::FirstGroupInclusive => String::from("ćlwó"),
                    VerbTypes::SecondGroup => String::from("eloh"),
                    VerbTypes::ThirdGroup => String::from("elwo"),
                    _ => panic!("Panic! {:?} is not a supported pronoun.", b.0),
                };

                write!(
                    f,
                    "{}·{}",
                    pronoun,
                    (&**b.1)
                        .into_iter()
                        .map(|x| format!("{}", x))
                        .collect::<Vec<String>>()
                        .join("·")
                )
            }
            Category::Negation => write!(f, "ni"),
            Category::Definite => write!(f, "[y]ða"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
enum AdjectiveTypes {
    Inalienable,
    Alienable,
}

impl Display for AdjectiveTypes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            AdjectiveTypes::Inalienable => write!(f, "[u]ki"),
            AdjectiveTypes::Alienable => write!(f, "[y]ćna"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
enum PluralTypes {
    Group,
    Regular,
}

impl Display for PluralTypes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            PluralTypes::Group => write!(f, "lwó"),
            PluralTypes::Regular => write!(f, "li"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
enum VerbTypes {
    Infinitive,
    FirstSingular,
    FirstPluralNonInclusive,
    FirstPluralInclusive,
    FirstGroupNonInclusive,
    FirstGroupInclusive,
    SecondSingular,
    SecondSingularVague,
    SecondPlural,
    SecondPluralVague,
    SecondGroup,
    ThirdSingular,
    ThirdPlural,
    ThirdPluralVague,
    ThirdGroup,
}

impl fmt::Display for VerbTypes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            VerbTypes::Infinitive => write!(f, "ú"),
            VerbTypes::FirstSingular => write!(f, "ÿźa"),
            VerbTypes::FirstPluralNonInclusive => write!(f, "ÿcy"),
            VerbTypes::FirstPluralInclusive => write!(f, "ÿća"),
            VerbTypes::SecondSingular => write!(f, "ÿta"),
            VerbTypes::SecondSingularVague => write!(f, "ymók"),
            VerbTypes::SecondPlural => write!(f, "ÿli"),
            VerbTypes::SecondPluralVague => write!(f, "ymŷk"),
            VerbTypes::ThirdSingular => write!(f, "wa"),
            VerbTypes::ThirdPlural => write!(f, "lŷ"),
            VerbTypes::ThirdPluralVague => write!(f, "śá"),
            VerbTypes::FirstGroupNonInclusive => write!(f, "ÿcó"),
            VerbTypes::FirstGroupInclusive => write!(f, "ÿćlwó"),
            VerbTypes::SecondGroup => write!(f, "ÿlwóh"),
            VerbTypes::ThirdGroup => write!(f, "lwó"),
        }
    }
}

fn main() { }

#[cfg(test)]
mod tests {
    #[test]
    fn word_to_string() {
        use crate::{Atom, Category, VerbTypes, Word};
        let word = match Word::new(vec![
            Atom::new(Category::Root("dav")),
            Atom::new(Category::Verb(VerbTypes::FirstSingular)),
            Atom::new(Category::Negation),
        ]) {
            Ok(w) => w,
            Err(e) => panic!("Paniced! got {:#?}", e),
        };
        assert_eq!("nidavÿźa", format!("{}", word));
    }

    #[test]
    fn infix_subject() {
        use crate::{AdjectiveTypes, Atom, Category, VerbTypes, Word};
        let word = Word::create(vec![
            Atom::new(Category::Verb(VerbTypes::ThirdSingular)),
            Atom::new(Category::Composite(Box::new(vec![
                Word::create(vec![
                    Atom::new(Category::Adjective(AdjectiveTypes::Inalienable)),
                    Atom::new(Category::Root("bolś")),
                ]),
                Word::create(vec![
                    Atom::new(Category::Root("mwoś")),
                    Atom::new(Category::Definite),
                ]),
            ]))),
            Atom::new(Category::Root("dav")),
            Atom::new(Category::Negation),
        ]);
        // "The cat doesn't love."
        assert_eq!("nidav·bolśki·mwośða·wa", format!("{}", word))
    }

    #[test]
    fn sentence_string() {
        use crate::{AdjectiveTypes, Atom, Category, Sentence, VerbTypes, Word};
        let sen = Sentence::new(vec![
            Word::create(vec![
                Atom::new(Category::Verb(VerbTypes::ThirdSingular)),
                Atom::new(Category::Composite(Box::new(vec![
                    Word::create(vec![
                        Atom::new(Category::Adjective(AdjectiveTypes::Inalienable)),
                        Atom::new(Category::Root("bolś")),
                    ]),
                    Word::create(vec![
                        Atom::new(Category::Root("mwoś")),
                        Atom::new(Category::Definite),
                    ]),
                ]))),
                Atom::new(Category::Root("dav")),
                Atom::new(Category::Negation),
            ]),
            Word::create(vec![Atom::new(Category::Root("źe"))]),
        ]);
        // "The big cat doesn't love me."
        assert_eq!("Nidav·bolśki·mwośða·wa źe.", format!("{}", sen))
    }
    #[test]
    fn possessive_stentence() {
        use crate::{AdjectiveTypes, Atom, Category, Sentence, VerbTypes, Word};
        let sen = Sentence::new(vec![
            Word::create(vec![
                Atom::new(Category::Verb(VerbTypes::ThirdSingular)), // "-wa"
                Atom::new(Category::Root("ðodym")), // "ðodymú" (to like)
                Atom::new(Category::Negation), // "ni-"
                Atom::new(Category::Composite(Box::new(vec![Word::create(vec![
                    Atom::new(Category::Possessive((
                        VerbTypes::FirstGroupInclusive, // "ćlwó·" (our [group inclusive]) 
                        Box::new(vec![
                            Word::create(vec![
                                Atom::new(Category::Adjective(AdjectiveTypes::Inalienable)), //"-ki"
                                Atom::new(Category::Root("bolś")), // "bolśki" (big)
                            ]),
                            Word::create(vec![Atom::new(Category::Root("mwoś"))]), // mwoś (cat)
                        ]),
                    ))),
                ])]))),
            ]),
            Word::create(vec![
                Atom::new(Category::Adjective(AdjectiveTypes::Inalienable)), // "-ki"
                Atom::new(Category::Root("kras")), // "kraski" (pretty/beautiful/handsome)
            ]),
            Word::create(vec![
                Atom::new(Category::Root("ŷmija")), // "ŷmija" (fox)
                Atom::new(Category::Definite), // "-ða" (the)
            ]),
        ]);
                // "Our big cat does not like the beautiful fox."
        assert_eq!("Niðodym·ćlwó·bolśki·mwoś·wa kraski ŷmijaða.", format!("{}", sen))
    }
}
